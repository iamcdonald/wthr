const app = require('../page-objects/app');

module.exports = function () {
  this.Then('I can see the page', done => {
    const appPage = app.create(browser);
    appPage.isVisible();
    done();
  });
  this.Then(/I can see a five day forecast for (.*)/, (city, done) => {
    const appPage = app.create(browser);
    appPage.hasForecastForCity(city);
    done();
  });
};
