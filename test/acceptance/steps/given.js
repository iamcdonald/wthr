const app = require('../page-objects/app');

module.exports = function () {
  this.Given('I visit the page', done => {
    const appPage = app.create(browser);
    appPage.visit();
    done();
  });
};
