const forecast = require('./forecast');

const LANDING_PAGE = '/';

const app = {
  _init: browser => {
    this._browser = browser;
    this._forecast = forecast.create(browser, '[data-test=app]');
  },
  visit: () => {
    this._browser.url(LANDING_PAGE);
  },
  isVisible: () => {
    this._browser.waitForVisible('[data-test=app]', 15000);
  },
  hasForecastForCity: city => {
    this._forecast.hasLoaded();
    this._forecast.isVisible();
    this._forecast.hasCityDisplayed(city);
    this._forecast.hasPredictionsForNextFiveDays(city);
  }
};

const create = browser => {
  const appPage = Object.create(app);
  appPage._init(browser);
  return appPage;
};

module.exports = {
  create
};
