const assert = require('assert');

const city = {
  _init: (browser, cityName, ctx) => {
    this._browser = browser;
    this._cityName = cityName;
    this._ctx = ctx;
    this._base = `${this._ctx} [data-test=city]`;
  },
  isVisible: () => {
    this._browser.isVisible(this._base);
    const cityName = this._browser.getText(`${this._base} [data-test=name]`);
    assert.equal(cityName, this._cityName);
  }
};

const create = (browser, cityName, ctx) => {
  const cityPage = Object.create(city);
  cityPage._init(browser, cityName, ctx);
  return cityPage;
};

module.exports = {
  create
};
