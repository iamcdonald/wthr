const assert = require('assert');

const WEEKDAYS = [
  'Sunday',
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday'
];

const pad = num => val => {
  const strVal = String(val);
  if (strVal.length < num) {
    const padding = Array.apply(null, { length: num - strVal.length }).map(() => '0').join('');
    return `${padding}${strVal}`;
  }
  return strVal;
};
const padTwo = pad(2);
const getDateString = date => `${padTwo(date.getUTCDate())}/${padTwo(date.getUTCMonth() + 1)}`;

const day = {
  _init: (browser, date, ctx) => {
    this._browser = browser;
    this._date = date;
    this._ctx = ctx;
    this._base = `${this._ctx} [data-test=day]`;
  },
  isVisible: () => {
    this._browser.isVisible(this._base);
    const dayString = this._browser.getText(`${this._base} [data-test=dayString]`);
    assert.equal(dayString, WEEKDAYS[this._date.getUTCDay()]);
    const date = this._browser.getText(`${this._base} [data-test=date]`);
    assert.equal(date, getDateString(this._date));
  }
};

const create = (browser, date, ctx) => {
  const dayPage = Object.create(day);
  dayPage._init(browser, date, ctx);
  return dayPage;
};

module.exports = {
  create
};
