const city = require('./city');
const groupedPredictions = require('./groupedPredictions');

const getToday = () => {
  const today = new Date();
  if (today.getUTCHours() >= 21) {
    today.setUTCDate(today.getUTCDate() + 1);
  }
  today.setUTCHours(0, 0, 0, 0);
  return today;
};

const getNextDay = date => {
  const next = new Date(date);
  next.setUTCDate(next.getUTCDate() + 1);
  return next;
};

const getNextFiveDays = () => {
  return Array.apply(null, { length: 5 })
    .reduce(acc => {
      if (!acc.length) {
        return acc.concat(getToday());
      }
      return acc.concat(getNextDay(acc[acc.length - 1]));
    }, []);
};

const forecast = {
  _init: (browser, ctx) => {
    this._browser = browser;
    this._ctx = ctx;
    this._base = `${this._ctx} [data-test=forecast]`;
  },
  hasLoaded: () => {
    this._browser.waitForVisible(this._base, 100000);
  },
  isVisible: () => {
    this._browser.isVisible(this._base);
  },
  hasCityDisplayed: cityName => {
    const cityPage = city.create(this._browser, cityName, this._base);
    cityPage.isVisible();
  },
  hasPredictionsForNextFiveDays: () => {
    getNextFiveDays().forEach(date => {
      const gpPage = groupedPredictions.create(this._browser, date, this._base);
      gpPage.isVisible();
    });
  }
};

const create = (browser, ctx) => {
  const forecastPage = Object.create(forecast);
  forecastPage._init(browser, ctx);
  return forecastPage;
};

module.exports = {
  create
};
