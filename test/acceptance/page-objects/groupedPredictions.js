const day = require('./day');
const assert = require('assert');

const getStringFromDate = date => `${date.getUTCFullYear()}${date.getUTCMonth()}${date.getUTCDate()}`;

const groupedPredictions = {
  _init: (browser, date, ctx) => {
    this._browser = browser;
    this._date = date;
    this._ctx = ctx;
    this._base = `${this._ctx} [data-test=group-${getStringFromDate(this._date)}]`;
    this._day = day.create(browser, date, this._base);
  },
  isVisible: () => {
    this._browser.isVisible(this._base);
    this._day.isVisible();
    const predictionsCount = this._browser.elements(`${this._base} [data-test=prediction]`).value.length;
    assert.ok(predictionsCount >= 1 && predictionsCount <= 8);
  }
};

const create = (browser, date, ctx) => {
  const gpPage = Object.create(groupedPredictions);
  gpPage._init(browser, date, ctx);
  return gpPage;
};

module.exports = {
  create
};
