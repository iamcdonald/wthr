import ava from 'ava';
import sinon from 'sinon';
import proxyquire from 'proxyquire';
import React from 'react';
import fakeBrowser from './test-utils/fakeBrowser';

proxyquire.noCallThru();

const setup = () => {
  fakeBrowser.setup();
  global.document.getElementById = sinon.stub().withArgs('app').returns('#app html element');
  const stubs = {
    'react-dom': {
      render: sinon.stub()
    },
    './style.css': {},
    './components/App': () => <span>app component</span>
  };
  proxyquire('../../src/index', stubs);
  return {
    stubs
  };
};

const teardown = () => {
  fakeBrowser.teardown();
};

ava('bootstraps application', t => {
  const { stubs } = setup();
  const App = stubs['./components/App'];
  t.true(stubs['react-dom'].render.calledWith(<App/>, '#app html element'));
  teardown();
});
