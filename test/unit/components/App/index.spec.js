import test from 'ava';
import proxyquire from 'proxyquire';
import sinon from 'sinon';
import { shallow } from 'enzyme';
import React from 'react';

proxyquire.noCallThru();

const setup = () => {
  const stubs = {
    './style.css': {
      app: 'app'
    },
    '../Forecast': () => {},
    '../../service/weather': {
      getForecastForCity: sinon.stub().returns(Promise.resolve('forecastData'))
    }
  };
  const App = proxyquire('../../../../src/components/App/index', stubs).default;
  return {
    stubs,
    App
  };
};

test('has correct initial state', t => {
  const { App } = setup();
  const appComp = new App();
  const expected = {
    pending: false,
    success: false,
    failure: false,
    forecast: null
  };
  t.deepEqual(appComp.state, expected);
});

test('getForecastForCity : sets initial state', t => {
  const { App } = setup();
  const appComp = new App();
  const cityID = '12345';
  const setStateStub = sinon.stub(appComp, 'setState');
  appComp.getForecastForCity(cityID);
  t.truthy(setStateStub.calledWith({
    pending: true,
    success: false,
    failure: false
  }));
});

test('getForecastForCity : calls for forecast data', t => {
  const { stubs, App } = setup();
  const appComp = new App();
  const cityID = '12345';
  sinon.stub(appComp, 'setState');
  appComp.getForecastForCity(cityID);
  t.truthy(stubs['../../service/weather'].getForecastForCity.calledWith(cityID));
});

test('getForecastForCity : when call resolves : sets correct tate', t => {
  const { App } = setup();
  const appComp = new App();
  const cityID = '12345';
  const setStateStub = sinon.stub(appComp, 'setState');
  return appComp.getForecastForCity(cityID)
    .then(() => {
      t.truthy(setStateStub.calledWith({
        pending: false,
        success: true,
        failure: false,
        forecast: 'forecastData'
      }));
    });
});

test('getForecastForCity : when call rejects : sets correct state', t => {
  const { stubs, App } = setup();
  stubs['../../service/weather'].getForecastForCity.returns(Promise.reject());
  const appComp = new App();
  const cityID = '12345';
  const setStateStub = sinon.stub(appComp, 'setState');
  return appComp.getForecastForCity(cityID)
    .catch(() => {
      t.truthy(setStateStub.calledWith({
        pending: false,
        success: false,
        failure: true,
        forecast: null
      }));
    });
});

test('componentWillMount : calls for forecast data for glasgow', t => {
  const { stubs, App } = setup();
  const appComp = new App();
  sinon.stub(appComp, 'setState');
  appComp.componentWillMount();
  t.truthy(stubs['../../service/weather'].getForecastForCity.calledWith('3333231'));
});

test('render : when forecast data avilable : display Forecast component', t => {
  const { stubs, App } = setup();
  const appWrapper = shallow(<App/>);
  const Forecast = stubs['../Forecast'];
  const forecastData = 'forecastData';
  appWrapper.setState({ forecast: forecastData });
  t.true(appWrapper.contains(<Forecast data={forecastData}/>));
});

test('render : when forecast data not avilable : does not display Forecast component', t => {
  const { stubs, App } = setup();
  stubs['../../service/weather'].getForecastForCity.returns(Promise.reject());
  const appWrapper = shallow(<App/>);
  const Forecast = stubs['../Forecast'];
  t.false(appWrapper.contains(<Forecast data={null}/>));
});
