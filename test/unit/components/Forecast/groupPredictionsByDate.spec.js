import test from 'ava';
import groupPredictionsByDate from '../../../../src/components/Forecast/groupPredictionsByDate';
import forecastData from '../../fixtures/forecast.json';
import forecastMapper from '../../../../src/mappers/forecast';

const forecastModel = forecastMapper.fromData(forecastData);

test('it groups predictions by date', t => {
  const expected = [
    {
      date: new Date(Date.UTC(2016, 9, 9)),
      predictions: forecastModel.predictions.slice(0, 8)
    },
    {
      date: new Date(Date.UTC(2016, 9, 10)),
      predictions: forecastModel.predictions.slice(8, 16)
    },
    {
      date: new Date(Date.UTC(2016, 9, 11)),
      predictions: forecastModel.predictions.slice(16, 24)
    },
    {
      date: new Date(Date.UTC(2016, 9, 12)),
      predictions: forecastModel.predictions.slice(24, 32)
    },
    {
      date: new Date(Date.UTC(2016, 9, 13)),
      predictions: forecastModel.predictions.slice(32, 40)
    }
  ];
  const grouped = groupPredictionsByDate(forecastModel.predictions);
  t.deepEqual(grouped, expected);
});
