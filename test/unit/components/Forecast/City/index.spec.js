import test from 'ava';
import proxyquire from 'proxyquire';
import { shallow } from 'enzyme';
import React from 'react';
import forecastData from '../../../fixtures/forecast.json';
import forecastMapper from '../../../../../src/mappers/forecast';

const forecastModel = forecastMapper.fromData(forecastData);

proxyquire.noCallThru();

const setup = () => {
  const stubs = {
    './style.css': {}
  };
  const City = proxyquire('../../../../../src/components/Forecast/City/index', stubs).default;
  return {
    stubs,
    City
  };
};

test('it displays city name', t => {
  const { City } = setup();
  const cityWrapper = shallow(<City data={forecastModel.city}/>);
  t.truthy(cityWrapper.contains(forecastModel.city.name));
});

test('it displays city country', t => {
  const { City } = setup();
  const cityWrapper = shallow(<City data={forecastModel.city}/>);
  t.truthy(cityWrapper.contains(forecastModel.city.country));
});
