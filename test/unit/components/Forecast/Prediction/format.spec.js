import test from 'ava';
import format from '../../../../../src/components/Forecast/Prediction/format';

test('temp : it returns formatted temperature', t => {
  const formatted = format.temp(12.2);
  const expected = '12.2°C';
  t.deepEqual(formatted, expected);
});

test('time : it returns formatted timew from date', t => {
  let date = new Date(2012, 1, 1, 2, 34);
  let formatted = format.time(date);
  let expected = '02:00';
  t.deepEqual(formatted, expected);
  date = new Date(2012, 1, 1, 22, 1);
  formatted = format.time(date);
  expected = '22:00';
  t.deepEqual(formatted, expected);
  date = new Date(2012, 1, 1, 6, 0);
  formatted = format.time(date);
  expected = '06:00';
  t.deepEqual(formatted, expected);
});
