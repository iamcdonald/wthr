import test from 'ava';
import proxyquire from 'proxyquire';
import { shallow } from 'enzyme';
import React from 'react';
import forecastData from '../../../fixtures/forecast.json';
import forecastMapper from '../../../../../src/mappers/forecast';
import format from '../../../../../src/components/Forecast/Prediction/format';

proxyquire.noCallThru();

const forecastModel = forecastMapper.fromData(forecastData);

const setup = () => {
  const stubs = {
    './style.css': {}
  };
  const Prediction = proxyquire('../../../../../src/components/Forecast/Prediction/index', stubs).default;
  return {
    stubs,
    Prediction
  };
};

test('it displays time', t => {
  const { Prediction } = setup();
  const pred = forecastModel.predictions[0];
  const predictionWrapper = shallow(<Prediction data={pred}/>);
  t.truthy(predictionWrapper.contains(format.time(pred.dateTime)));
});

test('it displays temp', t => {
  const { Prediction } = setup();
  const pred = forecastModel.predictions[0];
  const predictionWrapper = shallow(<Prediction data={pred}/>);
  t.truthy(predictionWrapper.contains(format.temp(pred.temp)));
});

test('it displays description', t => {
  const { Prediction } = setup();
  const pred = forecastModel.predictions[0];
  const predictionWrapper = shallow(<Prediction data={pred}/>);
  t.truthy(predictionWrapper.contains(pred.description));
});
