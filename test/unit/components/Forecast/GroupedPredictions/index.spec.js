import test from 'ava';
import proxyquire from 'proxyquire';
import { shallow } from 'enzyme';
import React from 'react';
import forecastData from '../../../fixtures/forecast.json';
import forecastMapper from '../../../../../src/mappers/forecast';
import groupPredictionsByDate from '../../../../../src/components/Forecast/groupPredictionsByDate';

proxyquire.noCallThru();

const forecastModel = forecastMapper.fromData(forecastData);
const datePredictions = groupPredictionsByDate(forecastModel.predictions)[0];

const setup = () => {
  const stubs = {
    './style.css': {},
    '../Prediction': () => {},
    '../Day': () => {}
  };
  const GroupedPredictions = proxyquire('../../../../../src/components/Forecast/GroupedPredictions/index', stubs).default;
  return {
    stubs,
    GroupedPredictions
  };
};

test('it renders day component', t => {
  const { stubs, GroupedPredictions } = setup();
  const Day = stubs['../Day'];
  const groupedPredWrapper = shallow(<GroupedPredictions date={datePredictions.date} predictions={datePredictions.predictions}/>);
  t.truthy(groupedPredWrapper.contains(<Day date={datePredictions.date}/>));
});

test('it renders prediction component for each prediction', t => {
  const { stubs, GroupedPredictions } = setup();
  const Prediction = stubs['../Prediction'];
  const groupedPredWrapper = shallow(<GroupedPredictions date={datePredictions.date} predictions={datePredictions.predictions}/>);
  datePredictions.predictions.forEach((pred, idx) => {
    t.truthy(groupedPredWrapper.contains(<Prediction key={idx} data={pred}/>));
  });
});
