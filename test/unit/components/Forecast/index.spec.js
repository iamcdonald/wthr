import test from 'ava';
import proxyquire from 'proxyquire';
import { shallow } from 'enzyme';
import React from 'react';
import forecastData from '../../fixtures/forecast.json';
import forecastMapper from '../../../../src/mappers/forecast';
import groupPredictionsByDate from '../../../../src/components/Forecast/groupPredictionsByDate';

const forecastModel = forecastMapper.fromData(forecastData);

proxyquire.noCallThru();

const setup = () => {
  const stubs = {
    './style.css': {},
    './City': () => {},
    './GroupedPredictions': () => {}
  };
  const Forecast = proxyquire('../../../../src/components/Forecast/index', stubs).default;
  return {
    stubs,
    Forecast
  };
};

test('it renders city component', t => {
  const { stubs, Forecast } = setup();
  const City = stubs['./City'];
  const forecastWrapper = shallow(<Forecast data={forecastModel}/>);
  t.truthy(forecastWrapper.contains(<City data={forecastModel.city}/>));
});

test('it renders a Day component for each set of predictions grouped by date', t => {
  const { stubs, Forecast } = setup();
  const GroupedPredictions = stubs['./GroupedPredictions'];
  const groupedByDatePredictions = groupPredictionsByDate(forecastModel.predictions);
  const forecastWrapper = shallow(<Forecast data={forecastModel}/>);
  groupedByDatePredictions.forEach((predictionsForDay, idx) => {
    t.truthy(forecastWrapper.contains(<GroupedPredictions key={idx} date={predictionsForDay.date} predictions={predictionsForDay.predictions}/>));
  });
});
