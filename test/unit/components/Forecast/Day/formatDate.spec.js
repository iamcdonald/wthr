import test from 'ava';
import formatDate from '../../../../../src/components/Forecast/Day/formatDate';

test('getDayString : it returns day', t => {
  const formatted = formatDate.getDayString(new Date(Date.UTC(2012, 10, 8)));
  const expected = 'Thursday';
  t.deepEqual(formatted, expected);
});

test('getAsDate : it returns formatted date', t => {
  let formatted = formatDate.getAsDate(new Date(Date.UTC(2012, 10, 8)));
  let expected = '08/11';
  t.deepEqual(formatted, expected);
  formatted = formatDate.getAsDate(new Date(Date.UTC(2012, 1, 2)));
  expected = '02/02';
  t.deepEqual(formatted, expected);
});
