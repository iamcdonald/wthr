import test from 'ava';
import proxyquire from 'proxyquire';
import { shallow } from 'enzyme';
import React from 'react';
import formatDate from '../../../../../src/components/Forecast/Day/formatDate';

proxyquire.noCallThru();

const setup = () => {
  const stubs = {
    './style.css': {},
    './Prediction': () => {}
  };
  const Day = proxyquire('../../../../../src/components/Forecast/Day/index', stubs).default;
  return {
    stubs,
    Day
  };
};

test('it displays date', t => {
  const { Day } = setup();
  const date = new Date(Date.UTC(2012, 9, 8));
  const dayWrapper = shallow(<Day date={date}/>);
  t.truthy(dayWrapper.contains(formatDate.getDayString(date)));
  t.truthy(dayWrapper.contains(formatDate.getAsDate(date)));
});
