import test from 'ava';
import cityMapper from '../../../src/mappers/city';
import predictionMapper from '../../../src/mappers/prediction';
import forecastMapper from '../../../src/mappers/forecast';
import forecastData from '../fixtures/forecast.json';

test('fromData : maps forecast to forecast object', t => {
  const forecastObj = forecastMapper.fromData(forecastData);
  t.deepEqual(forecastObj.city, cityMapper.fromData(forecastData.city));
  t.deepEqual(forecastObj.predictions, forecastData.list.map(predictionMapper.fromData));
});
