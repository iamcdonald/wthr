import test from 'ava';
import predictionMapper from '../../../src/mappers/prediction';
import forecastData from '../fixtures/forecast.json';

test('fromData : maps prediction data to prediction object', t => {
  const predictionData = forecastData.list[0];
  const predictionObj = predictionMapper.fromData(predictionData);
  t.deepEqual(predictionObj.dateTime, new Date(predictionData.dt * 1000));
  t.is(predictionObj.temp, predictionData.main.temp);
  t.is(predictionObj.wind.direction, predictionData.wind.deg);
  t.is(predictionObj.wind.speed, predictionData.wind.speed);
  t.is(predictionObj.description, predictionData.weather[0].description);
  t.is(predictionObj.id, predictionData.weather[0].id);
});
