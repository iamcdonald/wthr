import test from 'ava';
import cityMapper from '../../../src/mappers/city';
import forecastData from '../fixtures/forecast.json';

test('fromData : maps cityData to city object', t => {
  const cityObj = cityMapper.fromData(forecastData.city);
  t.is(cityObj.name, forecastData.city.name);
  t.is(cityObj.country, forecastData.city.country);
});
