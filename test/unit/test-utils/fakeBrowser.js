import jsdom from 'jsdom';

const setup = () => {
  global.document = jsdom.jsdom('');
  global.window = global.document.defaultView;
  global.navigator = { userAgent: 'node' };
};

const teardown = () => {
  delete global.document;
  delete global.window;
  delete global.navigator;
};

module.exports = {
  setup,
  teardown
};
