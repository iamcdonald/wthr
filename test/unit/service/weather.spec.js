import test from 'ava';
import sinon from 'sinon';
import proxyquire from 'proxyquire';
import forecastData from '../fixtures/forecast.json';
import forecastMapper from '../../../src/mappers/forecast';

const setup = () => {
  const stubs = {
    jsonp: sinon.stub()
  };
  const testee = proxyquire('../../../src/service/weather', stubs);
  return {
    stubs,
    testee
  };
};

const isPromise = val => typeof val.then === 'function' && typeof val.catch === 'function';

test('getForecastForCity : it should make jsonp call for forecast for given city ID', t => {
  const { stubs, testee } = setup();
  testee.getForecastForCity('1234567');
  t.truthy(stubs.jsonp.calledWith('http://api.openweathermap.org/data/2.5/forecast?id=1234567&units=metric&APPID=b4f2423074068e4e24e8427e3bf0ed81'));
  testee.getForecastForCity('99999');
  t.truthy(stubs.jsonp.calledWith('http://api.openweathermap.org/data/2.5/forecast?id=99999&units=metric&APPID=b4f2423074068e4e24e8427e3bf0ed81'));
});

test('getForecastForCity : it should return promise', t => {
  const { testee } = setup();
  t.truthy(isPromise(testee.getForecastForCity('127836')));
});

test('getForecastForCity : resolves with mapped data received from call', t => {
  const { stubs, testee } = setup();
  const promise = testee.getForecastForCity('99999')
    .then(forecast => {
      t.deepEqual(forecast, forecastMapper.fromData(forecastData));
    });
  stubs.jsonp.args[0][2](null, forecastData);
  return promise;
});

test('getForecastForCity : rejects with error if call fails', t => {
  const { stubs, testee } = setup();
  const forecastCallError = new Error('call failed');
  const promise = testee.getForecastForCity('99999')
    .catch(err => {
      t.deepEqual(err, forecastCallError);
    });
  stubs.jsonp.args[0][2](forecastCallError);
  return promise;
});
