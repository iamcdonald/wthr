import test from 'ava';
import prediction from '../../../src/models/prediction';

test('create: returns empty prediction object', t => {
  const predictionObj = prediction.create();
  t.is(predictionObj.dateTime, null);
  t.is(predictionObj.temp, null);
  t.is(predictionObj.wind.direction, null);
  t.is(predictionObj.wind.speed, null);
  t.is(predictionObj.description, null);
  t.is(predictionObj.id, null);
});
