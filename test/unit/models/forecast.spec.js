import test from 'ava';
import forecast from '../../../src/models/forecast';

test('create: returns empty forecast object', t => {
  const forecastObj = forecast.create();
  t.is(forecastObj.city, null);
  t.deepEqual(forecastObj.predictions, []);
});
