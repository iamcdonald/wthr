import test from 'ava';
import city from '../../../src/models/city';

test('create: returns empty city object', t => {
  const cityObj = city.create();
  t.is(cityObj.name, null);
  t.is(cityObj.country, null);
});
