# wthr
a live 5 day forecast available to view at [http://iamcdonald.gitlab.io/wthr/](http://iamcdonald.gitlab.io/wthr/)

```sh
> npm run lint                  //lints both test and src js files  
> npm run test:unit             //run unit tests  
> npm run test                  //list and run unit tests with coverage  
> npm run test:acceptance:local //run acceptance tests locally  
> npm run test:acceptance       //run acceptance tests across several browsers using SauceLabs (SAUCE_USER & SAUCE_PASS env variables required).
> npm start                     //run application locally  
> npm run build                 //build application into public directory  

```

### Tech Debt
- I'm not super happy about the component creep within [Forecast](https://gitlab.com/iamcdonald/wthr/blob/master/src/components/Forecast/index.js) and
 [GroupedPredictions](https://gitlab.com/iamcdonald/wthr/blob/master/src/components/Forecast/GroupedPredictions/index.js). I would most likely pull from
the ```ol``` down into a couple of components that were simply responsible for rendering a list of ```GroupedPredictions``` or ```Prediction``` relatively.


### Future work

- Make the UI a little more appealing
- Add ability to search for city or use longitude & latitude. This should be fairly easy,
  however would probably prompt me to bring in redux in to manage the interactions and data.
