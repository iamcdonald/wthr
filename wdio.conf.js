'use strict';
const localConfig = require('./wdio.conf.local.js').config;
const override = {
    user: process.env.SAUCE_USER,
    key: process.env.SAUCE_PASS,
    sauceConnect: true,
    capabilities: [
      {
        browserName: 'firefox'
      },
      {
        browserName: 'chrome'
      },
      {
        browserName: 'internet explorer'
      },
      {
        browserName: 'safari',
        platform: 'OS X 10.11'
      }
    ],
    services: ['sauce']
};
exports.config = Object.assign({}, localConfig, override);
