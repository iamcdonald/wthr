import jsonp from 'jsonp';
import forecastMapper from '../mappers/forecast';

const replace = (text, toReplace) => val => text.replace(toReplace, val);

const GET_FORECAST_FOR_CITY_URL = 'http://api.openweathermap.org/data/2.5/forecast?id=${cityID}&units=metric&APPID=b4f2423074068e4e24e8427e3bf0ed81';
const getForecastForCityURLWithCityID = replace(GET_FORECAST_FOR_CITY_URL, /\${cityID}/);

const getForecastForCity = cityID => {
  return new Promise((resolve, reject) => {
    jsonp(getForecastForCityURLWithCityID(cityID), null, (err, forecast) => {
      if (err) {
        return reject(err);
      }
      resolve(forecastMapper.fromData(forecast));
    });
  });
};

module.exports = {
  getForecastForCity
};
