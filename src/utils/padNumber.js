const padNumber = pad => num => {
  const stringNum = String(num);
  if (stringNum.length < pad) {
    const padding = Array.apply(null, { length: pad - stringNum.length }).map(() => 0).join('');
    return `${padding}${stringNum}`;
  }
  return stringNum;
};

module.exports = padNumber;
