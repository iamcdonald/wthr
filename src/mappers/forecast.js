import forecast from '../models/forecast';
import cityMapper from './city';
import predictionMapper from './prediction';

const fromData = data => {
  const forecastObj = forecast.create();
  forecastObj.city = cityMapper.fromData(data.city);
  forecastObj.predictions = data.list.map(predictionMapper.fromData);
  return forecastObj;
};

module.exports = {
  fromData
};
