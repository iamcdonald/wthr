import prediction from '../models/prediction';

const fromData = data => {
  const predictionObj = prediction.create();
  predictionObj.dateTime = new Date(data.dt * 1000);
  predictionObj.temp = data.main.temp;
  predictionObj.wind.speed = data.wind.speed;
  predictionObj.wind.direction = data.wind.deg;
  predictionObj.description = data.weather[0].description;
  predictionObj.id = data.weather[0].id;
  return predictionObj;
};

module.exports = {
  fromData
};
