import city from '../models/city';

const fromData = data => {
  const cityObj = city.create();
  cityObj.name = data.name;
  cityObj.country = data.country;
  return cityObj;
};

module.exports = {
  fromData
};
