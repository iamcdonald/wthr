import style from './style.css';
import React from 'react';
import Forecast from '../Forecast';
import weatherService from '../../service/weather';

const GLASGOW_CITY_ID = '3333231';

const App = {
  getInitialState() {
    return {
      pending: false,
      success: false,
      failure: false,
      forecast: null
    };
  },
  getForecastForCity(cityID) {
    this.setState({
      pending: true,
      success: false,
      failure: false
    });
    return weatherService.getForecastForCity(cityID)
      .then(forecast => {
        this.setState({
          pending: false,
          success: true,
          failure: false,
          forecast
        });
      })
      .catch(() => {
        this.setState({
          pending: false,
          success: false,
          failure: true
        });
      });
  },
  componentWillMount() {
    this.getForecastForCity(GLASGOW_CITY_ID);
  },
  render() {
    const { forecast } = this.state;
    return (
      <div data-test="app" className={style.app}>
        { forecast && <Forecast data={forecast}/> }
      </div>
    );
  }
};

export default React.createClass(App);
