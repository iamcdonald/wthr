const getDateFromPrediction = pred => {
  const date = new Date(pred.dateTime);
  date.setUTCHours(0, 0, 0, 0);
  return date;
};

const datesNotEqual = (date1, date2) => {
  return date1.toISOString() !== date2.toISOString();
};

const groupPredictionsByDate = predictions => {
  return predictions.reduce((acc, pred) => {
    const predDate = getDateFromPrediction(pred);
    if (!acc.length || (acc.length && datesNotEqual(acc[acc.length - 1].date, predDate))) {
      acc.push({
        date: predDate,
        predictions: [pred]
      });
    } else {
      acc[acc.length - 1].predictions.push(pred);
    }
    return acc;
  }, []);
};

module.exports = groupPredictionsByDate;
