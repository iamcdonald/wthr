import padNumber from '../../../utils/padNumber';

const temp = temp => `${temp}°C`;

const padTwo = padNumber(2);
const time = date => `${padTwo(date.getUTCHours())}:00`;

module.exports = {
  temp,
  time
};
