import React from 'react';
import format from './format';
import style from './style.css';

const Prediction = {
  render() {
    const { data: prediction } = this.props;
    return (
      <div data-test="prediction">
        <div className={style.time}>{format.time(prediction.dateTime)}</div>
        <div>{format.temp(prediction.temp)}</div>
        <div>{prediction.description}</div>
      </div>
    );
  }
};

export default React.createClass(Prediction);
