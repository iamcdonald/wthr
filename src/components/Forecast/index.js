import React from 'react';
import City from './City';
import GroupedPredictions from './GroupedPredictions';
import groupPredictionsByDate from './groupPredictionsByDate';

const Forecast = {
  render() {
    const { data: forecast } = this.props;
    const predictionsGroupedByDate = groupPredictionsByDate(forecast.predictions);
    return (
      <div data-test="forecast">
        <City data={forecast.city}/>
        <ol>
          {
            predictionsGroupedByDate.map((datePreds, idx) => {
              return (
                <li key={idx}>
                  <GroupedPredictions date={datePreds.date} predictions={datePreds.predictions}/>
                </li>
              );
            })
          }
        </ol>
      </div>
    );
  }
};

export default React.createClass(Forecast);
