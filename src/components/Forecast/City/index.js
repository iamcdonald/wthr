import React from 'react';
import style from './style.css';

const City = {
  render() {
    const { data: city } = this.props;
    return (
      <div data-test="city" className={style.city}>
        <div data-test="name">{city.name}</div>
        <div className={style.country}>{city.country}</div>
      </div>
    );
  }
};

export default React.createClass(City);
