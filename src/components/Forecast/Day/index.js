import React from 'react';
import formatDate from './formatDate';
import style from './style.css';

const Day = {
  render() {
    const { date } = this.props;
    return (
      <div data-test="day" className={style.day}>
        <span data-test="dayString" className={style.weekday}>{formatDate.getDayString(date)}</span>
        <span data-test="date">{formatDate.getAsDate(date)}</span>
      </div>
    );
  }
};

export default React.createClass(Day);
