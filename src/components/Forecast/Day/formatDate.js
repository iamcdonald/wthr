import padNumber from '../../../utils/padNumber';

const WEEKDAYS = [
  'Sunday',
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday'
];

const padTwo = padNumber(2);

const getDayString = date => WEEKDAYS[date.getUTCDay()];

const getAsDate = date => `${padTwo(date.getUTCDate())}/${padTwo(date.getUTCMonth() + 1)}`;

module.exports = {
  getDayString,
  getAsDate
};
