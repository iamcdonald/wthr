import React from 'react';
import Day from '../Day';
import Prediction from '../Prediction';
import style from './style.css';

const getStringFromDate = date => `${date.getUTCFullYear()}${date.getUTCMonth()}${date.getUTCDate()}`;

const GroupedPredictions = {
  render() {
    const { date, predictions } = this.props;
    return (
      <div data-test={`group-${getStringFromDate(date)}`} className={style.group}>
        <Day date={date}/>
        <ol>
          {
            predictions.map((pred, idx) => {
              return (
                <li key={idx} className={style.predictionListElement}>
                  <Prediction data={pred}/>
                </li>
              );
            })
          }
        </ol>
      </div>
    );
  }
};

export default React.createClass(GroupedPredictions);
