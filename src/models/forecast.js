const forecast = {
  city: null,
  predictions: []
};

const create = () => {
  return Object.assign({}, forecast);
};

module.exports = {
  create
};
