const prediction = {
  dateTime: null,
  temp: null,
  wind: {
    direction: null,
    speed: null
  },
  description: null,
  id: null
};

const create = () => {
  return Object.assign({}, prediction);
};

module.exports = {
  create
};
