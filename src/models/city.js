const city = {
  name: null,
  country: null
};

const create = () => {
  return Object.assign({}, city);
};

module.exports = {
  create
};
